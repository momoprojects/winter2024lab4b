import java.util.Scanner;

public class Driver {
	
	public static void main (String [] args) {
		
		Scanner reader= new Scanner(System.in); 
		Dinosaur gaggle [] = new Dinosaur [1];
		
		//construct dinos
		for (int i = 0; i<gaggle.length; i++) {
			System.out.println("Enter a name, height, and coolness of a dinosaur");
			String name = reader.next();
			double height = reader.nextDouble();
			int coolness = reader.nextInt();
			
			gaggle[i] = new Dinosaur(name, height, coolness);
		}
		Dinosaur lastDino= gaggle[gaggle.length-1];
		
		//this loop prints attributes of lastDino , then asks the user to change the name.
		//After the change, print the attributes again
		String trackTime= "before";
		int i=0;
		while (i<2){
			System.out.println("Attributes of last dinosaur "+trackTime+" change: Name:"
			+lastDino.getName()+
			" Height:"+lastDino.getHeight()+
			" Coolness:"+
			lastDino.getCoolness()+".");
			if (trackTime.equals("before")){
				System.out.println("Enter a new name for the last dinosaur:");
				lastDino.setName(reader.next());
				trackTime="after";
				i++;
			}
			else i++;
		}
		
		
	}
}