public class Dinosaur {
	private String name;
	private double height;
	private int coolness;
	
	public Dinosaur (String name, double height, int coolness) {
		this.name = name;
		this.height = height;
		this.coolness = coolness;
	}
	
	public void Greeting() {
		System.out.println("Welcome to my ted talk");
		System.out.println("A " + name + ", has a coolness of " + coolness);
	}
	
	public void HowManyHumansTall() {
		//average humans height according to google (1.77m)
		double numOfHum = height/1.77;
		System.out.println("A " + name + ", is " + numOfHum + " humans tall");
		System.out.println("Thank you for coming to my ted talk");
	}
	public String getName(){
		return this.name;
	}
	public double getHeight(){
		return this.height;
	}
	public int getCoolness(){
		return this.coolness;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
}